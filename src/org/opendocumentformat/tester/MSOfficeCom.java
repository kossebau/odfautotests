package org.opendocumentformat.tester;

import java.io.File;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

public class MSOfficeCom {
	static void convertOdt(String inputDoc, String outputDoc, int fileFormat) {
		final boolean saveOnExit = false;
		final boolean wordVisible = false;
		ActiveXComponent word = new ActiveXComponent("Word.Application");
		try {
			word.setProperty("Visible", new Variant(wordVisible));
			Dispatch documents = word.getProperty("Documents").toDispatch();
			Dispatch document = Dispatch.call(documents, "Open", inputDoc)
					.toDispatch();
			try {
				Dispatch.call(document, "SaveAs2", new Variant(outputDoc),
						new Variant(fileFormat));
			} finally {
				Dispatch.put(document, "Saved", true);
				Dispatch.call(document, "Close", new Variant(saveOnExit));
			}
		} finally {
			Dispatch.call(word, "Quit");
		}
	}

	static void convertOds(String inputDoc, String outputDoc, int fileFormat) {
		final boolean saveOnExit = false;
		final boolean excelVisible = false;
		ActiveXComponent excel = new ActiveXComponent("Excel.Application");
		try {
			excel.setProperty("Visible", new Variant(excelVisible));
			Dispatch documents = excel.getProperty("Workbooks").toDispatch();
			Dispatch document = Dispatch.call(documents, "Open", inputDoc)
					.toDispatch();
			new File(outputDoc).delete();
			try {
				if (fileFormat == excelPdfFormat) {
					// make sure that there is some content, otherwise excel
					// will complain that there is nothing to print
					Dispatch sheet = Dispatch.get(document, "ActiveSheet")
							.toDispatch();
					Dispatch cell = Dispatch.invoke(sheet, "Range",
							Dispatch.Get, new Object[] { "A1" }, new int[1])
							.toDispatch();
					String value = Dispatch.get(cell, "Value").getString();
					if (value == null || "".equals(value)) {
						Dispatch.put(cell, "Value", " ");
					}
					Dispatch.call(document, "ExportAsFixedFormat", new Variant(
							fileFormat), new Variant(outputDoc));
				} else {
					Dispatch.call(document, "SaveAs", new Variant(outputDoc),
							new Variant(fileFormat));
				}
			} finally {
				Dispatch.put(document, "Saved", true);
				Dispatch.call(document, "Close", new Variant(saveOnExit));
			}
		} finally {
			Dispatch.call(excel, "Quit");
		}
	}

	static void convertOdp(String inputDoc, String outputDoc, int fileFormat) {
		final boolean embedFonts = true;
		ActiveXComponent powerpoint = new ActiveXComponent(
				"Powerpoint.Application");
		try {
			Dispatch documents = powerpoint.getProperty("Presentations")
					.toDispatch();
			Dispatch document = Dispatch.call(documents, "Open", inputDoc)
					.toDispatch();
			new File(outputDoc).delete();
			try {
				Dispatch.call(document, "SaveAs", new Variant(outputDoc),
						new Variant(fileFormat), new Variant(embedFonts));
			} finally {
				Dispatch.put(document, "Saved", true);
				Dispatch.call(document, "Close");
			}
		} finally {
			Dispatch.call(powerpoint, "Quit");
		}
	}

	static final int wordPdfFormat = 17;
	static final int wordOdtFormat = 23;
	static final int excelOdsFormat = 60;
	static final int excelPdfFormat = 0;
	static final int powerpointPdfFormat = 32;
	static final int powerpointOdfFormat = 35;

}
